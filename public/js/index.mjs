import { Application } from "./bundle/pixi.mjs";
import telaFinalizacao from "./telaFinalizacao.mjs";
import mudarTela from "./mudarTela.mjs";
import criarCeuNoturno from "./ceu.mjs";

import telaInicial from "./telaInicial.mjs"

const app = new Application({
  resizeTo: window,
  resolution: window.devicePixelRatio || 1,
  autoDensity: true
});

window.app = app;

document.body.appendChild(app.view);

const tamanho = {
  width: app.screen.width,
  height: app.screen.height,
};

telaInicial(app, tamanho)
//const ceuNoturno = criarCeuNoturno(tamanho)
//const telaFinal = telaFinalizacao(tamanho)
//app.stage.addChild(ceuNoturno, telaFinal)
