import { Graphics, filters } from "./bundle/pixi.mjs";

/**
 * @param {{width: number, height: number}} tamanhoTela
 * @returns { Graphics }
 */
function criarCeuNoturno(tamanhoTela) {
  const ceuNoturno = new Graphics();
  ceuNoturno.beginFill(0x000025);
  ceuNoturno.drawRect(0, 0, tamanhoTela.width, tamanhoTela.height);
  ceuNoturno.endFill();

  const quantidadeEstrelas = (tamanhoTela.width * 2 + tamanhoTela.height * 2) / 3

  ceuNoturno.beginFill(0xffffff);
  for (let i = 0; i < quantidadeEstrelas; i++) {
    let posX = Math.random() * app.screen.width;
    let posY = Math.random() * app.screen.height;
    
    let rng = Math.random();
    let tamanho = -Math.log(1 - rng) * 1.2;
    ceuNoturno.drawStar(
      posX,
      posY,
      4,
      tamanho,
    )

  }
  ceuNoturno.endFill();
  ceuNoturno.filters = [
    new PIXI.filters.AdvancedBloomFilter({ quality: 5, blur: 4 }),
  ];
  ceuNoturno.cacheAsBitmap = true;

  return ceuNoturno;
}

export default criarCeuNoturno
