import { Sprite, Graphics, Texture, Container, Text, TextStyle } from "./bundle/pixi.mjs";
import mudarTela from "./mudarTela.mjs";
import geradorInstancia from "./geradorFase.mjs";
import {modoLivia} from "./telaInicial.mjs";

/** @typedef {object} Constelacao
 * @property {object[]} estrelas
 * @property {number} estrelas.x
 * @property {number} estrelas.y
 * @property {number[][]} ligacoes
 * @property {string} nome
 */

/**
 * @param {Constelacao} constelacao
 * @param { () => void | undefined } callback
 */
function criarEstrelas(constelacao, callback) {
  const tamanhoTela = {
    width: window.innerWidth,
    height: window.innerHeight,
  }

  const containerConstelacao = new Container();

  const texturaEstrela = Texture.from("./estrela.svg");

  //faz com que o x e y normalizados ocupem a tela toda
  //e centraliza o container de estrelas no meio da tela
  let multiplicacaoTamanho;
  if (tamanhoTela.width > tamanhoTela.height) {
    multiplicacaoTamanho = tamanhoTela.height;
    containerConstelacao.x = (tamanhoTela.width - tamanhoTela.height) / 2;
  } else {
    multiplicacaoTamanho = tamanhoTela.width;
    containerConstelacao.y = (tamanhoTela.height - tamanhoTela.width) / 2;
  }

  const tamanho = ((tamanhoTela.width + tamanhoTela.height) / 2) / 25

  constelacao.estrelas.forEach((estrelaPos, indexEstrela) => {
    //comeca a colocar cada estrela
    const estrelaSprite = Sprite.from(texturaEstrela);
    containerConstelacao.addChild(estrelaSprite);

    estrelaSprite.anchor.set(0.5);
    estrelaSprite.x = Math.floor(estrelaPos.x * multiplicacaoTamanho);
    estrelaSprite.y = Math.floor(estrelaPos.y * multiplicacaoTamanho);
    estrelaSprite.width = tamanho
    estrelaSprite.height = tamanho

    estrelaSprite.buttonMode = true;
    estrelaSprite.interactive = true;

    estrelaSprite.on("pointerdown", () => {
      const ligacoesDessaEstrela = constelacao.ligacoes
        .filter((ligacao) => ligacao.includes(indexEstrela))
        .map((incluso) =>
          incluso[0] == indexEstrela ? incluso[1] : incluso[0]
        );
      console.log(indexEstrela)

      //caso todas ligacoes ja estejam feita, nao cria mais linha saindo da estrela
      if(ligacoesDessaEstrela.length === 0){
        return
      }

      containerConstelacao.interactive = true;

      const linha = new Graphics();
      containerConstelacao.addChildAt(linha, 0);

      const filtroEstrelaGlow = new PIXI.filters.GlowFilter({
        outerStrength: 0,
      });
      estrelaSprite.filters = [filtroEstrelaGlow];

      let distanciaEstrelaNormalizada = 0;
      let indexEstrelaMaisProxima = 0;

      estrelaSprite.on("pointerupoutside", pointerUp);
      estrelaSprite.on("pointerup", pointerUp);
      containerConstelacao.on("pointermove", moveHandle);
      app.ticker.add(girarEstrela);

      function girarEstrela(deltaTime) {
        estrelaSprite.angle =
          estrelaSprite.angle + distanciaEstrelaNormalizada * deltaTime * 4;
      }

      function moveHandle(e) {
        const { x: mouseX, y: mouseY } = e.data.global;

        linha.clear();
        linha.lineStyle(5, 0xffffff);
        linha.moveTo(estrelaSprite.x, estrelaSprite.y);
        linha.lineTo(
          mouseX - containerConstelacao.x,
          mouseY - containerConstelacao.y
        );

        //tem a certeza que ligacoesDessaEstrela esta populada, pela condicional la em cima
        const distanciaMaisProxima = ligacoesDessaEstrela
          .map((indexEstrelaLigada) => {
            const estrelaMedida = constelacao.estrelas[indexEstrelaLigada];
            const posicaoEstrelaLigadaY =
              estrelaMedida.y * multiplicacaoTamanho + containerConstelacao.y;
            const posicaoEstrelaLigadaX =
              estrelaMedida.x * multiplicacaoTamanho + containerConstelacao.x;

            const catetoMouse1 = Math.pow(mouseX - posicaoEstrelaLigadaX, 2);
            const catetoMouse2 = Math.pow(mouseY - posicaoEstrelaLigadaY, 2);
            const mouse = Math.sqrt(catetoMouse1 + catetoMouse2);

            const catetoEstrela1 = Math.pow(estrelaSprite.x - posicaoEstrelaLigadaX, 2)
            const catetoEstrela2 = Math.pow(estrelaSprite.y - posicaoEstrelaLigadaY, 2)
            const total = Math.sqrt(catetoEstrela1 + catetoEstrela2)

            return {
              mouse,
              total,
              indexEstrela: indexEstrelaLigada,
            };
          })
          .reduce((distanciaAnterior, distanciaAtual) =>
            distanciaAnterior.mouse < distanciaAtual.mouse ? distanciaAnterior : distanciaAtual
          );

        const distanciaNormalizada =
          1 - Math.min(distanciaMaisProxima.mouse / distanciaMaisProxima.total, 1);

        filtroEstrelaGlow.outerStrength = distanciaNormalizada * 6;

        distanciaEstrelaNormalizada = distanciaNormalizada;
        indexEstrelaMaisProxima = distanciaMaisProxima.indexEstrela
      }

      function pointerUp() {
        //quando parar perto da estrela certa
        if (distanciaEstrelaNormalizada > 0.85) {

          let ligacaoFeita = constelacao.ligacoes.find(
            (ligacao) =>
              ligacao.includes(indexEstrela) &&
              ligacao.includes(indexEstrelaMaisProxima)
          );

          linha.clear();
          linha.lineStyle(5, 0xffffff);
          linha.moveTo(estrelaSprite.x, estrelaSprite.y);
          linha.lineTo(
            constelacao.estrelas[indexEstrelaMaisProxima].x * multiplicacaoTamanho,
            constelacao.estrelas[indexEstrelaMaisProxima].y * multiplicacaoTamanho,
          );

          if (typeof ligacaoFeita !== "undefined") {
            let indexLigacaoFeita = constelacao.ligacoes.indexOf(ligacaoFeita);
            constelacao.ligacoes.splice(indexLigacaoFeita, 1);

            if(constelacao.ligacoes.length === 0){
              const estiloNomeEstrela = new TextStyle({
                fontFamily: "Brush Script MT",
                fontSize: Math.min(tamanhoTela.width / 10, tamanhoTela.height / 10),
                fill : 0x000095,
                stroke: 0xffffff,
                strokeThickness: 1,
                align : 'center',
                wordWrap: true,
                wordWrapWidth: tamanhoTela.width
              })
              console.log(constelacao.nome)
              const textoUsado = modoLivia ? "Constelação De\nCancêr" : "Constelação De\n" + constelacao.nome
              const textoNomeEstrela = new Text(textoUsado, estiloNomeEstrela)
              textoNomeEstrela.x = tamanhoTela.width / 2
              textoNomeEstrela.y = tamanhoTela.height / 2
              textoNomeEstrela.anchor.set(.5)
              textoNomeEstrela.alpha = 0

              app.stage.addChild(textoNomeEstrela)

              let tempoPassadoMS = 0
              app.ticker.add(function ticker(){
                const delta = app.ticker.elapsedMS
                tempoPassadoMS += delta

                if(tempoPassadoMS > 500){
                  app.ticker.remove(ticker)
                  return
                }

                textoNomeEstrela.alpha = tempoPassadoMS / 500
              })

              setTimeout(() => callback && callback(), 3000)
            }
          }

        } else {
          //se para longe de uma estrela certa
          containerConstelacao.removeChild(linha);
        }

        containerConstelacao.interactive = false;
        containerConstelacao.off("pointermove", moveHandle);
        estrelaSprite.off("pointerupoutside", pointerUp);
        estrelaSprite.off("pointerup", pointerUp);
        estrelaSprite.filters = [];
        app.ticker.remove(girarEstrela);
      }
    });
  });

  return containerConstelacao;
}

export default criarEstrelas;
