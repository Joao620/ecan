import criarCeuNoturno from "./ceu.mjs";
import passarFase from "./geradorFase.mjs";

export let modoLivia = window.location.hash === '#livia'

import { Application, Text, TextStyle, Container, Graphics  } from './bundle/pixi.mjs'

/**
 * @param {Application} app
 */
function telaInicial(app, tamanhoTela){
  const ceuNoturno = criarCeuNoturno(tamanhoTela)
  app.stage.addChild(ceuNoturno)

  const titulo = criarTitulo(tamanhoTela)
  const subtitulo = criarSubtitulo(tamanhoTela, titulo)
  const botaoInicar = criarBotaoIniciar(tamanhoTela, subtitulo, "Começar!", () => passarFase(app))

  app.stage.addChild(titulo, subtitulo, botaoInicar)
}

function criarTitulo(tamanhoTela){
  const tituloEstilo = new TextStyle({
    fontFamily: "Brush Script MT",
    fontSize: Math.min(tamanhoTela.width / 3, tamanhoTela.height / 3),
    fill : 0x000095,
    align : 'center',
    dropShadow: true,
    dropShadowColor: '#ffffff',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 3,
    
  })

  const titulo = new Text("Ecan", tituloEstilo)
  titulo.x = (tamanhoTela.width - titulo.width) / 2
  titulo.y = tamanhoTela.height / 10

  return titulo
}

function criarSubtitulo(tamanhoTela, titulo){
  const subtituloEstilo = new TextStyle({
    fontFamily: "Brush Script MT",
    fontSize: 32,
    fill : 0x000095,
    stroke: 0xffffff,
    strokeThickness: 1,
    align : 'center',
    wordWrap: true,
    wordWrapWidth: tamanhoTela.width
  })

  const subtitulo = new Text("Um jogo sobre ligar constelacoes", subtituloEstilo)
  subtitulo.anchor.set(0.5, 0)
  subtitulo.x = titulo.x + (titulo.width / 2)
  subtitulo.y = titulo.y + titulo.height

  return subtitulo
}


function criarBotaoIniciar(tamanhoTela, subtitulo, texto, callback){
  const textoIniciarEstilo = new TextStyle({
    fontFamily: "Brush Script MT",
    fontSize: 32,
    fill : 0x000095,
    stroke: 0xffffff,
    strokeThickness: 1,
    align : 'center',
  })

  const textoIniciar = new Text(texto, textoIniciarEstilo)
  textoIniciar.x = textoIniciar.width * .05
  textoIniciar.y = textoIniciar.height * .05

  const backgroundIniciar = new Graphics();
  backgroundIniciar.beginFill(0xffffff);
  backgroundIniciar.lineStyle(5,0x000025)
  backgroundIniciar.drawRect(0, 0, textoIniciar.width * 1.1, textoIniciar.height * 1.1);
  backgroundIniciar.endFill();

  const botaoIniciar = new Container()
  botaoIniciar.addChild(backgroundIniciar)
  botaoIniciar.addChild(textoIniciar)

  botaoIniciar.buttonMode = true
  botaoIniciar.interactive = true

  botaoIniciar.x = (tamanhoTela.width - botaoIniciar.width) / 2
  botaoIniciar.y = subtitulo.y + subtitulo.height

  botaoIniciar.on("pointerdown", callback)

  return botaoIniciar
}

export default telaInicial
