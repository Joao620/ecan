import { Application, Container } from './bundle/pixi.mjs'


/** @param { Application } app
 *  @param { Container[] } proximaTela
 *  @param { () => null | undefined } callback
*/
function mudarTela(app, proximaTela, callback){
  //TODO: fazer com que tenha um filtro na mudanca de tela
  const itensAntesAdicao = app.stage.children.length
  proximaTela.forEach((itemTela) => {
    itemTela.alpha = 0
    app.stage.addChild(itemTela)
  })

  let tempoPassadoMS = 0

  app.ticker.add(function ticker(){
    const delta = app.ticker.elapsedMS
    tempoPassadoMS += delta
    if(tempoPassadoMS >= 2000){
      app.ticker.remove(ticker)
      app.stage.removeChildren(0, itensAntesAdicao)
      callback && callback()
      return
    }

    const alphaAparecendo = tempoPassadoMS / 2000
    const alphaSumindo = 1 - alphaAparecendo

    let i = 0
    for(; i < itensAntesAdicao; i++){
      app.stage.children[i].alpha = alphaSumindo
    }

    for(; i < app.stage.children.length; i++){
      app.stage.children[i].alpha = alphaAparecendo
    }

  })
  
}

export default mudarTela
