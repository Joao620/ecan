import { TextStyle, Text } from './bundle/pixi.mjs'
function rot13(str) {
  var re = new RegExp("[a-z]", "i");
  var min = 'A'.charCodeAt(0);
  var max = 'Z'.charCodeAt(0);
  var factor = 13;
  var result = "";
  str = str.toUpperCase();
  
  for (var i=0; i<str.length; i++) {
    result += (re.test(str[i]) ?
      String.fromCharCode((str.charCodeAt(i) - min + factor) % (max-min+1) + min) : str[i]);
  }
  
  return result;
}

function telaFinalizacao(tamanhoTela){
  const estiloFinal = new TextStyle({
    fontFamily: "Brush Script MT",
    fontSize: Math.min(tamanhoTela.width / 15, tamanhoTela.height / 15),
    fill : 0x000095,
    stroke: 0xaaaaaa,
    strokeThickness: 2,
    align : 'center',
    wordWrap: true,
    wordWrapWidth: tamanhoTela.width
  })

  const textoFinal = new Text('Oi Boba, parabens por ter passado por todas contelações, tenho pouco espaço, então só quero dizer que gosto muito de você, e mesmo com nossas turbulências, e a suas piras com alguém me chamando de xuxu, eu vou pra sempre gostar de você', estiloFinal)
  textoFinal.x = tamanhoTela.width / 2
  textoFinal.y = tamanhoTela.height / 2
  textoFinal.anchor.set(.5)

  return textoFinal
}

export { rot13 }
export default telaFinalizacao
