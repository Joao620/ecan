import criarCeuNoturno from "./ceu.mjs";
import constelacoes from "./constelacoes.mjs";
import criarEstrelas from "./estrela.mjs";
import mudarTela from "./mudarTela.mjs";
import telaFinalizacao from "./telaFinalizacao.mjs";
import { modoLivia } from "./telaInicial.mjs";

function* geradorFase(){
  for (let i = constelacoes.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = constelacoes[j];
    constelacoes[j] = constelacoes[i];
    constelacoes[i] = temp;

  }

  for(let i = 0; i < constelacoes.length - 1; i++){
    yield constelacoes[i]
  }

  return constelacoes[constelacoes.length]
}

const geradorInstancia = geradorFase()

function passarFase(app){
  const tamanhoTela = {
    width: app.screen.width,
    height: app.screen.height,
  };

  const ceuNoturno = criarCeuNoturno(tamanhoTela)
  const constelacao = geradorInstancia.next().value

  if(!constelacao){
    const telaFinal = telaFinalizacao(tamanhoTela)
    mudarTela(app, [ceuNoturno, telaFinal])
    return
  }

  const estrelas = criarEstrelas(constelacao, () => passarFase(app))

  mudarTela(app, [ceuNoturno, estrelas])
}


export default passarFase
